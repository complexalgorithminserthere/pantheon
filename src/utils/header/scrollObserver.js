export const scrollObserver = app => {
  const distanceY = window.pageYOffset || document.documentElement.scrollTop;
  const height = 50;
  if (distanceY > height) {
    app.setState({ scrolled: true });
  } else {
    app.setState({ scrolled: false });
  }
};
