import React from 'react';
import Loader from '../loader/loader.component';

const WithSpinner = WrappedComponent => {
  const Spinner = ({ isLoading, ...otherProps }) => {
    return isLoading ? <Loader /> : <WrappedComponent {...otherProps} />;
  };
  return Spinner;
};

export default WithSpinner;
