import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import Loader from '../loader/loader.component';
import GameCard from '../game-card/game-card.component';
import { Games } from './games-grid.styles';
import './games-grid.styles.scss';

const GamesGrid = ({ games, fetch }) => (
  <Games>
    <InfiniteScroll
      className='games-wrapper'
      dataLength={games.length}
      hasMore={true}
      loader={<Loader />}
      next={fetch}
      scrollThreshold='10px'
    >
      {games.map(game => (
        <GameCard key={game.id} game={game} />
      ))}
    </InfiniteScroll>
  </Games>
);

export default GamesGrid;
