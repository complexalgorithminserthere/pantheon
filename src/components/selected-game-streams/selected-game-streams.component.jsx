import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectSelectedGameStreams,
  selectSelectedGame
} from '../../redux/games/games.selectors';
import { fetchSelectedGameStartAsync } from '../../redux/games/games.actions';
import {
  Container,
  Header,
  StickySentinel,
  LayoutRow,
  FlexContent,
  Title,
  ControlInput,
  SearchInput,
  SearchButton,
  SearchIcon,
  Icon,
  IconSpan,
  MobileButton,
  StreamsGrid
} from './selected-game-streams.styles';
import StreamCard from '../stream-card/stream-card.component';
import InfiniteScroll from 'react-infinite-scroll-component';
import Loader from '../loader/loader.component';
import '../top-streams/top-streams.styles.scss';

const TopStreams = ({ allStreams, game, fetchNewStreamsAsync }) => (
  <Container>
    <Header>
      <StickySentinel />
      <div>
        <LayoutRow>
          <FlexContent>
            <Title>{game.name} STREAMS</Title>
          </FlexContent>
          <ControlInput>
            <SearchInput placeholder={'Search Streams'} />
            <SearchButton>
              <SearchIcon>
                <Icon>
                  <IconSpan>
                    <i className='material-icons'>search</i>
                  </IconSpan>
                </Icon>
              </SearchIcon>
            </SearchButton>
          </ControlInput>
          <MobileButton>
            <SearchIcon>
              <Icon>
                <IconSpan>
                  <i className='material-icons'>search</i>
                </IconSpan>
              </Icon>
            </SearchIcon>
          </MobileButton>
        </LayoutRow>
      </div>
    </Header>
    <StreamsGrid>
      <InfiniteScroll
        className='streams-wrapper'
        dataLength={allStreams.length}
        hasMore={true}
        loader={<Loader />}
        next={() => fetchNewStreamsAsync(game.id)}
        scrollThreshold='10px'
      >
        {allStreams.map((stream, index) => (
          <StreamCard key={index} stream={stream} />
        ))}
      </InfiniteScroll>
    </StreamsGrid>
  </Container>
);

const mapStateToProps = createStructuredSelector({
  allStreams: selectSelectedGameStreams,
  game: selectSelectedGame
});

const mapDispatchToProps = dispatch => ({
  fetchNewStreamsAsync: id => dispatch(fetchSelectedGameStartAsync(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TopStreams);
