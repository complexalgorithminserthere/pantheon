import styled from 'styled-components';

export const Container = styled.div`
  display: block;
  min-height: 100vh;
  margin-top: 0px;

  @media (min-width: 960px) {
    margin-top: 48px;
  }
`;

export const Header = styled.header`
  transition: background-color 400ms cubic-bezier(0.35, 0, 0.25, 1);
  position: sticky;
  z-index: 9;
  /* background-color: rgba(33, 44, 61, 0); */
  background-color: #202225;
  display: block;

  &::after {
    transition: opacity 400ms cubic-bezier(0.35, 0, 0.25, 1);
    content: '';
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    background: radial-gradient(
      farthest-side at 50% 0,
      rgba(0, 0, 0, 0.5),
      transparent
    );
    opacity: 0;
    height: 60px;
    pointer-events: none;
    z-index: -1;
  }

  @media (max-width: 960px) {
    top: 47px;
  }

  @media (min-width: 960px) {
    top: 59px;
  }
`;

export const StickySentinel = styled.div`
  position: absolute;
  top: -1px;
  width: 1px;
  height: 1px;
  display: block;

  @media (max-width: 960px) {
    top: -49px;
  }

  @media (min-width: 960px) {
    top: -61px;
  }
`;

export const LayoutRow = styled.div`
  min-height: 60px;
  display: flex;
  flex-direction: row;
  align-content: center;
  align-items: center;
  max-width: 100%;
  justify-content: flex-start;
`;

export const FlexContent = styled.div`
  min-width: 0;
  flex: 1;
  max-width: 100%;
  display: block;
`;

export const Title = styled.h1`
  margin: 16px 0;
  font-size: 28px;
  color: #f7f7f7;
  margin-left: 4px;

  @media (max-width: 960px) {
    font-size: 20px;
  }

  @media (max-width: 1440px) {
    margin-left: 8px;
  }
`;

export const ControlInput = styled.div`
  position: relative;
  width: 300px;
  margin: 4px 16px;
  max-width: 100%;
  display: block;

  @media (max-width: 960px) {
    display: none;
    width: 100%;
  }
`;

export const SearchInput = styled.input`
  background: #131d2c;
  border-radius: 2px;
  outline: 0 !important;
  padding: 0.7em;
  font-size: 0.9em;
  color: #fff;
  border: 2px solid transparent;
  width: 100%;

  &:focus {
    border: 2px solid #00bcf2;
  }
`;

export const SearchButton = styled.button`
  position: absolute;
  top: 5%;
  right: 0;
  cursor: pointer;
  display: inline-block;
  border: none;
  background: transparent;
`;

export const MobileButton = styled.button`
  position: relative;
  border: none;
  cursor: pointer;
  background: transparent;

  @media (min-width: 960px) {
    display: none;
  }

  @media (max-width: 960px) {
    display: block;
  }
`;

export const SearchIcon = styled.div`
  background: transparent;
  color: #fff;
  width: 36px;
  min-width: 36px;
  border-radius: 50%;
  padding: 0;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  position: relative;
  overflow: hidden;
  height: 36px;
  outline: 0;
  border: none;
  line-height: 14px;
  font-size: 14px;
  font-weight: 500;
  text-transform: uppercase;
  cursor: pointer;

  &::before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
  }
`;

export const Icon = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  line-height: 0;
  text-align: center;
  transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const IconSpan = styled.span`
  color: #00bcf2;
  font-size: 24px;
  width: 1em;
  height: 1em;
  display: inline-block;
  overflow: hidden;
  line-height: 0;
  text-align: center;
`;

export const StreamsGrid = styled.div`
  display: block;

  @media (max-width: 960px) {
    margin: 4px;
  }
`;

export const StreamsWrapper = styled.div`
  height: auto;
  display: flex;
  flex-flow: row wrap;
`;
