import styled from 'styled-components';

export const ButtonContainer = styled.button`
  display: block;
  text-align: center;
  letter-spacing: 0.5px;
  cursor: pointer;
  background: #016f80;
  width: 100%;
  margin-top: 10px;
  color: #fff;
  height: 40px;
  border-radius: 3px;
  border: none;
  font-weight: 500;
  text-transform: uppercase;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
  padding: 0 16px;
  outline: 0;
  transition: 300ms ease all;

  &:hover {
    background-color: transparent;
    border: 2px solid #3cc8c8;
    color: #3cc8c8;
  }
`;
