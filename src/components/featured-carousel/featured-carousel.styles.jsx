import styled from 'styled-components';
import { device } from '../../utils/device/device';

export const CarouselContainer = styled.div`
  display: block;
`;

export const Header = styled.header`
  display: flex;
  align-items: center;
  margin-top: 16px;
  margin-right: 0;

  @media ${device.laptop} {
    margin-top: 4px;
  }
`;

export const HeaderTitle = styled.h1`
  margin: 16px 0;
  font-size: 28px;
  letter-spacing: 1px;
  color: #f7f7f7;
  margin-left: 8px;

  @media (max-width: 960px) {
    display: none;
    font-size: 20px;
  }

  @media ${device.laptopL} {
    margin-left: 4px;
  }
`;

export const Carousel = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
`;

export const LeftArrow = styled.button`
  position: absolute;
  left: 5px;
  top: 24px;
  bottom: 24px;
  background: rgba(17, 17, 17, 0.5);
  border: none;
  overflow: hidden;
  width: 48px;
  z-index: 1;
  cursor: pointer;

  &::before {
    position: absolute;
    content: '';
    top: 50%;
    left: 50%;
    width: 28px;
    height: 28px;
    border-left: 2px solid #00bcf2;
    border-bottom: 2px solid #00bcf2;
    transform: translate(-25%, -50%) rotate(45deg);
  }

  &:hover {
    background: rgba(17, 17, 17, 0.8);
  }

  &:focus {
    outline: none;
  }

  @media (max-width: 960px) {
    width: 24px;

    &::before {
      width: 14px;
      height: 14px;
    }
  }
`;

export const RightArrow = styled.button`
  position: absolute;
  right: 5px;
  top: 24px;
  bottom: 24px;
  background: rgba(17, 17, 17, 0.5);
  border: none;
  overflow: hidden;
  width: 48px;
  z-index: 1;
  cursor: pointer;

  &::before {
    position: absolute;
    content: '';
    top: 50%;
    left: 50%;
    width: 28px;
    height: 28px;
    border-left: 2px solid #00bcf2;
    border-bottom: 2px solid #00bcf2;
    transform: translate(-75%, -50%) rotate(-135deg);
  }

  &:hover {
    background: rgba(17, 17, 17, 0.8);
  }

  &:focus {
    outline: none;
  }

  @media (max-width: 960px) {
    width: 24px;

    &::before {
      width: 14px;
      height: 14px;
    }
  }
`;

export const FeaturedPanelOuter = styled.div`
  display: none;
  flex-shrink: 1;
  width: 230px;
  min-width: 0;

  position: relative;
  margin: 24px 5px;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  overflow: hidden;
  z-index: 0;

  &::before {
    content: '';
    background: linear-gradient(
      rgba(0, 0, 0, 0.8),
      rgba(0, 0, 0, 0.6),
      rgba(0, 0, 0, 0)
    );
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    height: 120px;
    z-index: 1;
  }

  @media ${device.laptop} {
    display: block;
  }
`;

export const FeaturedPanelInner = styled.div`
  display: block;
  width: 230px;
  min-width: 0;
  flex-shrink: 0;

  position: relative;
  margin: 24px 5px;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  overflow: hidden;
  z-index: 0;

  &::before {
    content: '';
    background: linear-gradient(
      rgba(0, 0, 0, 0.8),
      rgba(0, 0, 0, 0.6),
      rgba(0, 0, 0, 0)
    );
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    height: 120px;
    z-index: 1;
  }

  @media (max-width: 1132px) {
    flex-shrink: 1;
  }
`;

export const FeaturedPanelCenter = styled.div`
  position: relative;
  width: calc(100vh * 16 / 9 * 0.8 - 64px);
  flex-shrink: 0;
  max-width: calc(100vw - 64px);
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),
    0px 5px 8px 0px rgba(0, 0, 0, 0.14), 0px 1px 14px 0px rgba(0, 0, 0, 0.12);
  border: 2px solid #00bcf2;

  &::before {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    background: linear-gradient(
      rgba(0, 0, 0, 0.4),
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0),
      rgba(0, 0, 0, 0.4)
    );
    z-index: 1;
  }

  @media ${device.laptop} {
    width: 672px;
  }
`;

export const Sizer = styled.div`
  padding-top: 56.25%;
  position: relative;
  z-index: 2;
`;

export const Thumbnail = styled.img`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  height: 100%;
`;

export const ChannelInfo = styled.div`
  mask-image: linear-gradient(to left, transparent, #000 30px);
  -webkit-mask-image: linear-gradient(to left, transparent, #000 30px);
  position: absolute;
  z-index: 2;
  top: 24px;
  left: 16px;
  right: 0;
  overflow: hidden;
  color: #fff;
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
  max-width: 100%;
  justify-content: flex-start;
`;

export const Avatar = styled.div`
  width: 54px;
  height: 54px;
  display: block;
  position: relative;
  max-width: 100%;
`;

export const AvatarImg = styled.img`
  width: 54px;
  height: 54px;
  border-radius: 50%;
`;

export const Titles = styled.div`
  opacity: 0.75;
  margin-left: 8px;
  margin-bottom: 0;
  max-width: 100%;
  font-size: 14px;
  display: block;
`;

export const UserName = styled.div`
  font-size: 18px;
  display: block;
`;
