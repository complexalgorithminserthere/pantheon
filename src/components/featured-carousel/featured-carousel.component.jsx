import React from 'react';
import {
  CarouselContainer,
  Header,
  HeaderTitle,
  Carousel,
  LeftArrow,
  RightArrow,
  FeaturedPanelOuter,
  FeaturedPanelInner,
  FeaturedPanelCenter,
  Sizer,
  Thumbnail,
  ChannelInfo,
  Avatar,
  AvatarImg,
  Titles,
  UserName
} from './featured-carousel.styles';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectTopStreams } from '../../redux/streams/streams.selectors';
import { carouselNavigate } from '../../redux/streams/streams.actions';

import Stream from '../video-player/video-player.component';

const FeaturedCarousel = ({ navigate, topStreams }) => (
  <CarouselContainer>
    <Header>
      <HeaderTitle>Featured</HeaderTitle>
    </Header>
    <Carousel>
      <LeftArrow onClick={() => navigate(-1)} />
      <FeaturedPanelOuter>
        <ChannelInfo>
          <Avatar>
            <AvatarImg src={topStreams[0].avatar} />
          </Avatar>
          <Titles>
            <UserName>{topStreams[0].user_name}</UserName>
          </Titles>
        </ChannelInfo>
        <Thumbnail src={topStreams[0].thumbnail} />
      </FeaturedPanelOuter>
      <FeaturedPanelInner>
        <ChannelInfo>
          <Avatar>
            <AvatarImg src={topStreams[1].avatar} />
          </Avatar>
          <Titles>
            <UserName>{topStreams[1].user_name}</UserName>
          </Titles>
        </ChannelInfo>
        <Thumbnail src={topStreams[1].thumbnail} />
      </FeaturedPanelInner>
      <FeaturedPanelCenter>
        <Stream username={topStreams[2].user_name} center={true} />
        <Sizer />
      </FeaturedPanelCenter>
      <FeaturedPanelInner>
        <ChannelInfo>
          <Avatar>
            <AvatarImg src={topStreams[3].avatar} />
          </Avatar>
          <Titles>
            <UserName>{topStreams[3].user_name}</UserName>
          </Titles>
        </ChannelInfo>
        <Thumbnail src={topStreams[3].thumbnail} />
      </FeaturedPanelInner>
      <FeaturedPanelOuter>
        <ChannelInfo>
          <Avatar>
            <AvatarImg src={topStreams[4].avatar} />
          </Avatar>
          <Titles>
            <UserName>{topStreams[4].user_name}</UserName>
          </Titles>
        </ChannelInfo>
        <Thumbnail src={topStreams[4].thumbnail} />
      </FeaturedPanelOuter>
      <RightArrow onClick={() => navigate(1)} />
    </Carousel>
  </CarouselContainer>
);

const mapStateToProps = createStructuredSelector({
  topStreams: selectTopStreams
});

const mapDispatchToProps = dispatch => ({
  navigate: n => dispatch(carouselNavigate(n))
});

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedCarousel);
