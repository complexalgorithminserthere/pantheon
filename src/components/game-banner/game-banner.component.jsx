import React from 'react';
import { GameThumb, GameArt, Title } from './game-banner.styles';

const GameBanner = ({ game }) => (
  <GameThumb>
    <GameArt src={game.art} />
    <Title>{game.name}</Title>
  </GameThumb>
);

export default GameBanner;
