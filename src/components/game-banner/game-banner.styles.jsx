import styled from 'styled-components';

export const GameThumb = styled.div`
  display: flex;
  text-align: center;
  padding-top: 40px;
  flex-direction: column;
  align-items: center;
  align-content: center;
  justify-content: center;
  max-width: 100%;
`;

export const GameArt = styled.img`
  min-width: 128px;
  min-height: 128px;
  width: 40vw;
  height: 40vw;
  margin: 0 0 16px;
  box-shadow: 0 0 30px #000;
  max-width: 100%;

  @media (min-width: 960px) {
    width: 25vw;
    height: 25vw;
  }
`;

export const Title = styled.h1`
  font-weight: bold;
  color: #fff;
  margin: 0;
  display: block;
  font-size: 1.7em;
  opacity: 0.87;
`;
