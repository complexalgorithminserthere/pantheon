import React from 'react';

import './video-player.styles.css';

const Stream = ({ username, center }) => (
  <iframe
    title='main'
    className='video-player'
    src={`https://player.twitch.tv/?channel=${username}&parent=optimistic-sinoussi-ca1ee3.netlify.app`}
    scrolling='yes'
    frameBorder='0'
    allowFullScreen={true}
    width={center ? '100%' : '590px'}
  ></iframe>
);

export default Stream;
