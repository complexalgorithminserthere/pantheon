import React from 'react';
import { Backdrop } from './modal-conductor.styles';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import SignInModal from '../sign-in-modal/sign-in-modal.component';
import SignUpModal from '../sign-up-modal/sign-up-modal.component';
import { closeOnOutsideClick } from '../../redux/user/user.actions';
import { selectCurrentModal } from '../../redux/user/user.selectors';

const ModalConductor = ({ currentModal, closeModal }) => {
  const signUp = currentModal === 'sign-up';
  return (
    <Backdrop>
      {signUp ? (
        <SignUpModal closeModal={closeModal} />
      ) : (
        <SignInModal closeModal={closeModal} />
      )}
    </Backdrop>
  );
};

const mapStateToProps = createStructuredSelector({
  currentModal: selectCurrentModal
});

const mapDispatchToProps = dispatch => ({
  closeModal: () => dispatch(closeOnOutsideClick())
});
export default connect(mapStateToProps, mapDispatchToProps)(ModalConductor);
