import styled, { css } from 'styled-components';
import { NavLink } from 'react-router-dom';

const scrolledStyle = css`
  transform: translateY(0px);
  background: #2f3136;
`;

const getScrollStyles = props => {
  if (props.scrolled) {
    return scrolledStyle;
  }
};
export const UserName = styled.p`
  color: #fff;
  font-size: 15px;
  margin: 1em;
`;
export const UserImage = styled.img`
  width: 50px;
  height: 50px;
  margin: 1em 0;
  border-radius: 10em;
`;
export const OptionLink = styled(NavLink)`
  cursor: pointer;
  color: #ffffff;
  display: inline-flex;
  margin: 0 10px;
  letter-spacing: 2px;
  text-decoration: none;

  &.active {
    /* color: turquoise; */
    padding: 5px 0;
    border-bottom: 2px solid turquoise;
  }
`;

export const MobileSignIn = styled.button`
  background: none;
  border: none;
  display: inline-block;
  cursor: pointer;
  color: #ffffff;

  &:focus {
    outline: none;
  }

  @media (min-width: 600px) {
    display: none;
  }
`;

export const SignInButton = styled.button`
  background: #016f80;
  padding: 8px 20px;
  color: #ffffff;
  border: 1px solid transparent;
  cursor: pointer;
  border-radius: 4px;

  &:hover {
    opacity: 0.8;
  }

  &:focus {
    outline: none;
  }

  @media (max-width: 600px) {
    display: none;
  }
`;

export const SignUpButton = styled.button`
  background: #242729;
  padding: 8px 20px;
  color: #ffffff;
  border: 1px solid transparent;
  cursor: pointer;
  border-radius: 4px;
  margin: 0 10px;

  &:hover {
    opacity: 0.8;
  }

  &:focus {
    outline: none;
  }

  @media (max-width: 600px) {
    display: none;
  }
`;

export const NavContainer = styled.div`
  position: sticky;
  top: 0px;
  display: block;
  align-items: center;
  z-index: 49;
  height: 100px;
`;

export const NavContentContainer = styled.div`
  transition: transform 300ms cubic-bezier(0.25, 0.8, 0.25, 1),
    background 300ms cubic-bezier(0.25, 0.8, 0.25, 1);
  display: flex;
  align-items: center;
  width: 100%;
  height: 60px;
  transform: translateY(25px);
  pointer-events: auto;

  ${getScrollStyles}
`;

export const NavItemsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-items: center;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  margin: 0 16px;
  width: calc(100% - (16px * 2));
`;

export const NavItemsLeft = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const NavItemsRight = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const LogoContainer = styled.div`
  height: 100%;
  width: 50px;
  padding: 20px;
  margin-right: 50px;
`;

export const Logo = styled.img`
  width: 45px;
  height: 45px;
`;

Logo.displayName = 'Logo';
