import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  toggleModalVisible,
  launchSignUp
} from '../../redux/user/user.actions';
import Logoo from '../../assets/logo.png';
import {
  LogoContainer,
  NavContainer,
  NavContentContainer,
  NavItemsLeft,
  NavItemsRight,
  NavItemsContainer,
  OptionLink,
  SignInButton,
  UserImage,
  UserName,
  MobileSignIn,
  Logo
} from './header.styles';

const handleClick = event => {
  event.stopPropagation();
};

export const Header = ({ launchSignIn, launchSignUp, user, ...props }) => (
  <NavContainer>
    <NavContentContainer scrolled={props.scrolled}>
      <NavItemsContainer>
        <NavItemsLeft>
          <Link to='/'>
            <LogoContainer>
              <Logo src={Logoo} />
            </LogoContainer>
          </Link>
          <OptionLink to='/streams'>STREAMS</OptionLink>
          <OptionLink to='/games'>GAMES</OptionLink>
        </NavItemsLeft>
        <NavItemsRight>
          {user ? (
            <>
              <UserImage src={user.avatar} width='140' height='140' />
              <UserName>{user.name}</UserName>
              <SignInButton onClick={props.handleLogout}>Sign Out</SignInButton>
            </>
          ) : (
            <>
              <SignInButton onClick={launchSignIn}>Sign in</SignInButton>
              <MobileSignIn onClick={launchSignIn}>
                <i className='material-icons'>person</i>
              </MobileSignIn>
            </>
          )}
        </NavItemsRight>
      </NavItemsContainer>
    </NavContentContainer>
  </NavContainer>
);

const mapDispatchToProps = dispatch => ({
  launchSignIn: () => dispatch(toggleModalVisible()),
  launchSignUp: () => dispatch(launchSignUp())
});

export default connect(null, mapDispatchToProps)(Header);
