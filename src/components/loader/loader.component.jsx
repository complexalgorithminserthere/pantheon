import React from 'react';
import { SpinnerContainer, SpinnerOverlay } from './loader.styles';

const Loader = () => (
  <SpinnerOverlay>
    <SpinnerContainer />
  </SpinnerOverlay>
);

export default Loader;
