import styled from 'styled-components';

export const SignUpWrapper = styled.div``;

export const SignUpForm = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const SignUpContainer = styled.div`
  width: 300px;
  height: 450px;
  display: flex;
  background: #152b38;
  border-radius: 7px;
  flex-direction: column;
  justify-content: start;
  align-items: center;
  padding: 20px 30px;
`;

export const LogoSection = styled.div`
  width: 100%;
  height: 100px;
  padding: 0 0 0 70px;
`;

export const GoogleButton = styled.div`
  height: 40px;
  border-width: 0;
  width: 100%;
  margin-top: 0.5em;

  white-space: nowrap;
  box-shadow: 1px 1px 0px 1px rgba(0, 0, 0, 0.05);
  transition-property: background-color, box-shadow;
  transition-duration: 150ms;
  transition-timing-function: ease-in-out;
  padding: 0;
`;
export const GoogleButtonOuter = styled.button`
  display: inline-block;
  vertical-align: middle;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  cursor: pointer;
  border-radius: 3px;
  border: transparent;
  &:hover {
    background-color: transparent !important;
    border: 2px solid #fff;
  }
`;
export const GoogleSpanInner = styled.span`
  display: inline-block;
  vertical-align: middle;
  margin: 8px 0 8px 8px;
  width: 18px;
  height: 18px;
  box-sizing: border-box;
`;

export const GoogleSpanText = styled.span`
  display: inline-block;
  vertical-align: middle;
  color: #737373;
  padding: 0 24px;
  font-size: 14px;
  font-weight: bold;
  &:hover {
    color: #fff !important;
  }
`;
