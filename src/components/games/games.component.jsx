import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectFilteredGames,
  selectIsGamesLoaded
} from '../../redux/games/games.selectors';
import {
  fetchGamesStartAsync,
  resetGames
} from '../../redux/games/games.actions';
import withSpinner from '../with-spinner/with-spinner.component';
import GamesGrid from '../games-grid/games-grid.component';
const GamesGridWithSpinner = withSpinner(GamesGrid);

class Games extends React.Component {
  componentDidMount() {
    const { fetchGamesAsync } = this.props;
    fetchGamesAsync();
  }

  componentWillUnmount() {
    const { reset } = this.props;
    reset();
  }

  render() {
    const { games, fetchGamesAsync, isGamesLoaded } = this.props;
    return (
      <GamesGridWithSpinner
        games={games}
        fetch={fetchGamesAsync}
        isLoading={!isGamesLoaded}
      />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  games: selectFilteredGames,
  isGamesLoaded: selectIsGamesLoaded
});

const mapDispatchToProps = dispatch => ({
  fetchGamesAsync: () => dispatch(fetchGamesStartAsync()),
  reset: () => dispatch(resetGames())
});

export default connect(mapStateToProps, mapDispatchToProps)(Games);
