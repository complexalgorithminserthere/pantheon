import styled from 'styled-components';

export const Header = styled.header`
  display: flex;
  align-items: center;
  margin-top: 48px;
  margin-right: 8px;

  @media (max-width: 1440px) {
    margin-right: 0;
  }

  @media (max-width: 960px) {
    margin-top: 16px;
    position: sticky;
    top: -1px;
    background: #202225;
    margin-right: 0;
    z-index: 1;
  }
`;

export const HeaderTitle = styled.h1`
  margin: 16px 0;
  font-size: 28px;
  color: #f7f7f7;
  margin-left: 4px;

  @media (max-width: 1440px) {
    margin-left: 8px;
  }

  @media (max-width: 960px) {
    font-size: 20px;
  }
`;

export const Spacer = styled.span`
  flex: 1;
`;

export const HeaderButton = styled.button`
  background: transparent;
  display: inline-block;
  position: relative;
  border: none;
  cursor: pointer;
  color: #00bcf2; 
  font-size: 20px;
`;

export const GamesRow = styled.div`
  display: flex;

  @media (max-width: 1280px) {
    flex-wrap: wrap;
    margin: 4px;
  }
`;
