import React from 'react';
import { Header, HeaderTitle, Spacer, GamesRow } from './top-games.styles';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectTopGames } from '../../redux/top-games/top-games.selectors';
import GameCard from '../game-card/game-card.component';

const TopGames = ({ topGames }) => (
  <div>
    <Header>
      <HeaderTitle>TOP GAMES</HeaderTitle>
      <Spacer />
    </Header>
    <GamesRow>
      {topGames.map(game => (
        <GameCard key={game.id} game={game} />
      ))}
    </GamesRow>
  </div>
);

const mapStateToProps = createStructuredSelector({
  topGames: selectTopGames
});

export default connect(mapStateToProps)(TopGames);
