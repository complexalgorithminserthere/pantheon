import React from 'react';
import onClickOutside from 'react-onclickoutside';
import {
  SignInForm,
  SignInContainer,
  LogoSection,
  ForgotPassButton,
  TwitchButton,
  Logo
} from './sign-in-modal.styles';
import Logoo from '../../assets/logo.png';
import twitchLogo from '../../assets/twitch32.png';
import FormInput from '../form-input/input.component';
import CustomButton from '../custom-button/custom-button.component';

class SignInModal extends React.Component {
  state = {
    email: '',
    password: ''
  };

  handleClickOutside = evt => {
    // ..handling code goes here...
    this.props.closeModal();
  };

  handleChange = event => {
    const { value, name } = event.target;

    this.setState({ [name]: value });
  };

  handleClick = event => {
    event.stopPropagation();
    window.open('http://localhost:5000/auth/login', '_self');
  };

  render() {
    return (
      <SignInContainer>
        <LogoSection>
          <Logo src={Logoo} />
        </LogoSection>
        <SignInForm>
          <FormInput
            type='email'
            name='email'
            value={this.state.email}
            handleChange={this.handleChange}
            label='Email'
            required
          />
          <FormInput
            type='password'
            name='password'
            value={this.state.password}
            handleChange={this.handleChange}
            label='Password'
            required
          />
          <CustomButton value={'Log in'} />
          <TwitchButton onClick={this.handleClick}>
            SIGN IN WITH TWITCH
            <img src={twitchLogo} alt='' />
          </TwitchButton>
          <ForgotPassButton>FORGOT PASSWORD?</ForgotPassButton>
        </SignInForm>
      </SignInContainer>
    );
  }
}

export default onClickOutside(SignInModal);
