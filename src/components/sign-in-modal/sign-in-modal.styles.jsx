import styled from 'styled-components';

export const SignInForm = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const SignInContainer = styled.div`
  width: 300px;
  height: 450px;
  display: flex;
  background: #152b38;
  border-radius: 7px;
  flex-direction: column;
  justify-content: start;
  align-items: center;
  padding: 20px 30px;
`;

export const LogoSection = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ForgotPassButton = styled.button`
  display: block;
  text-align: center;
  letter-spacing: 0.5px;
  cursor: pointer;
  background: none;
  width: 100%;
  margin-top: 10px;
  color: #fff;
  height: 40px;
  border-radius: 3px;
  border: none;
  font-weight: 500;
  text-transform: uppercase;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
  padding: 0 16px;
  outline: 0;
  transition: 300ms ease all;

  &:hover {
    background: rgba(255, 255, 255, 0.2);
  }
`;

export const TwitchButton = styled.button`
  display: inline-flex;
  text-align: center;
  align-items: center;
  justify-content: center;
  letter-spacing: 0.5px;
  cursor: pointer;
  background: #6441a5;
  width: 100%;
  margin-top: 10px;
  color: #fff;
  height: 40px;
  border-radius: 3px;
  border: none;
  font-weight: 500;
  text-transform: uppercase;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
  padding: 0 16px;
  outline: 0;
  transition: 500ms ease all;

  &:hover {
    background: none;
    color: #6441a5;
    border: 2px solid #6441a5;
    justify-content: space-between;
  }
`;

export const Logo = styled.img`
  width: 45px;
  height: 45px;
`;
