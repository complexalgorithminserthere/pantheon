import React from 'react';
import { Wrapper } from './stream.styles';
import ReactTwitchEmbedVideo from 'react-twitch-embed-video';
import './stream.styles.scss';

const Stream = ({ match }) => {
  return (
    <Wrapper>
      <ReactTwitchEmbedVideo
        targetClass='twitch-embed'
        channel={`${match.params.channel}`}
        width='100%'
        theme='dark'
      />
    </Wrapper>
  );
};

export default Stream;
