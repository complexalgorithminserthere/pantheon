import styled from 'styled-components';

export const AR = styled.div`
  padding-top: 56.25%;
  position: relative;
  overflow: hidden;
`;

export const Wrapper = styled.div`
  margin: 0 auto;
  width: 90%;
`;
