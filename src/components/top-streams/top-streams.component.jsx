import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  Container,
  Header,
  StickySentinel,
  LayoutRow,
  FlexContent,
  Title,
  ControlInput,
  SearchInput,
  SearchButton,
  SearchIcon,
  Icon,
  IconSpan,
  MobileButton,
  StreamsGrid
} from './top-streams.styles';
import StreamCard from '../stream-card/stream-card.component';
import { selectNewStreams } from '../../redux/streams/streams.selectors';
import { fetchNewStreamsStartAsync } from '../../redux/streams/streams.actions';
import InfiniteScroll from 'react-infinite-scroll-component';
import Loader from '../loader/loader.component';
import './top-streams.styles.scss';

const TopStreams = ({ allStreams, fetchNewStreamsAsync }) => (
  <Container>
    <Header>
      <StickySentinel />
      <div>
        <LayoutRow>
          <FlexContent>
            <Title>TOP STREAMS</Title>
          </FlexContent>
          <ControlInput>
            <SearchInput placeholder={'Search Streams'} />
            <SearchButton>
              <SearchIcon>
                <Icon>
                  <IconSpan>
                    <i className='material-icons'>search</i>
                  </IconSpan>
                </Icon>
              </SearchIcon>
            </SearchButton>
          </ControlInput>
          <MobileButton>
            <SearchIcon>
              <Icon>
                <IconSpan>
                  <i className='material-icons'>search</i>
                </IconSpan>
              </Icon>
            </SearchIcon>
          </MobileButton>
        </LayoutRow>
      </div>
    </Header>
    <StreamsGrid>
      <InfiniteScroll
        className='streams-wrapper'
        dataLength={allStreams.length}
        hasMore={true}
        loader={<Loader />}
        next={fetchNewStreamsAsync}
        scrollThreshold='10px'
      >
        {allStreams.map((stream, index) => (
          <StreamCard key={index} stream={stream} />
        ))}
      </InfiniteScroll>
    </StreamsGrid>
  </Container>
);

const mapStateToProps = createStructuredSelector({
  allStreams: selectNewStreams
});

const mapDispatchToProps = dispatch => ({
  fetchNewStreamsAsync: () => dispatch(fetchNewStreamsStartAsync())
});

export default connect(mapStateToProps, mapDispatchToProps)(TopStreams);
