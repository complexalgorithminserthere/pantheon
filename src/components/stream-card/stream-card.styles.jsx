import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Card = styled.div`
  width: 0;
  flex-basis: 25%;

  @media (max-width: 960px) {
    flex-basis: 100%;
  }
`;

export const CardContent = styled(Link)`
  position: relative;
  border-radius: 6px;
  background: #2f3136;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  display: block;
  overflow: hidden;
  margin: 4px;
  color: #fff;

  text-decoration: none;
  transition: color 150ms;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border: 2px solid transparent;
    z-index: 1;
    border-radius: 6px;
  }

  @media (max-width: 960px) {
    display: flex;
    flex-direction: row;
    align-items: start;
    border-radius: 0;
    box-shadow: none;
    background: none !important;
  }
`;

export const Thumbnail = styled.div`
  background: #000;
  position: relative;
  display: block;

  &::before {
    display: block;
    content: '';
    padding-top: 56.25%;
  }

  @media (max-width: 960px) {
    flex-basis: 42%;
    width: 42%;
    max-width: 300px;
  }
`;

export const Image = styled.img`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  max-height: 100%;
  max-width: 100%;
`;

export const LiveAndViewers = styled.div`
  display: flex;
  position: absolute;
  top: 12px;
  left: 8px;

  z-index: 8;

  @media (max-width: 960px) {
    top: 4px;
    left: 4px;
  }
`;

export const LiveIndicator = styled.div`
  display: flex;
  margin-right: 4px;
  z-index: 8;
`;

export const LiveIndicatorItems = styled.div`
  display: flex;
  background: rgba(17, 17, 17, 0.8);
  border-radius: 4px;
  padding: 0 4px;
  align-items: center;
`;

export const RedDot = styled.span`
  height: 8px;
  width: 8px;
  background-color: #df158a;
  border-radius: 50%;
  margin-left: 4px;
  display: inline-block;
`;

export const LiveString = styled.span`
  color: #fff;
  font-family: 'Segoe UI', sans-serif;
  font-size: 13px;
  font-weight: 600;
  line-height: 17px;
  text-transform: uppercase;
  margin-left: 4px;
`;

export const ViewersBadge = styled.div`
  display: flex;
  align-items: center;
  font-size: 0.9em;
  background: rgba(17, 17, 17, 0.8);
  border-radius: 3px;
  padding: 4px 8px;
`;

export const EyesIcon = styled.div`
  display: flex;
  align-items: center;
  position: relative;
  margin-top: 0.09rem;
  font-size: 0.9em;
  margin-right: 0.3em;
  width: 1em;
  height: 1em;
  overflow: hidden;
`;

export const Footer = styled.div`
  display: flex;
  padding: 16px;
  align-items: center;
  flex-grow: 1;

  @media (max-width: 960px) {
    padding: 0 8px 8px;
    width: 0;
  }
`;

export const Avatar = styled.div`
  width: 52px;
  height: 52px;
  display: block;
  position: relative;

  @media (max-width: 960px) {
    display: none;
  }
`;

export const AvatarImg = styled.img`
  border-radius: 50%;
  width: 52px;
  height: 52px;
`;

export const Titles = styled.div`
  margin-left: 8px;
  display: flex;
  flex-grow: 1;
  width: 0;
  flex-direction: column;
  align-items: flex-start;
  align-content: flex-start;
  justify-content: center;
`;

export const StreamTitle = styled.h2`
  font-size: 0.9em;
  font-weight: 500;
  width: 100%;
  overflow: hidden;
  margin-bottom: 0.1em;
  display: block;
`;

export const TitleString = styled.span`
  display: inline-block;
  position: relative;
  white-space: nowrap;
  text-overflow: ellipsis;
  width: 100%;
  overflow: hidden;
  line-height: 1.25em;
`;

export const StreamerName = styled.small`
  display: block;
  font-size: 0.8em;
  opacity: 0.6;
  line-height: 1.1em;
  margin-top: 0.15em;
  overflow: hidden;
`;

export const GameName = styled.small`
  display: block;
  font-size: 0.8em;
  opacity: 0.6;
  line-height: 1.1em;
  margin-top: 0.15em;
  overflow: hidden;
`;

export const GameString = styled.span`
  display: inline-block;
  position: relative;
  white-space: nowrap;
  text-overflow: ellipsis;
  width: 100%;
  overflow: hidden;
  line-height: 1.25em;
`;
