import React from 'react';
import {
  Card,
  CardContent,
  Thumbnail,
  Image,
  LiveAndViewers,
  LiveIndicator,
  LiveIndicatorItems,
  RedDot,
  LiveString,
  ViewersBadge,
  EyesIcon,
  Footer,
  Avatar,
  AvatarImg,
  Titles,
  StreamTitle,
  TitleString,
  StreamerName,
  GameName,
  GameString
} from './stream-card.styles';

const StreamCard = ({ stream }) => (
  <Card>
    <CardContent to={`/stream/${stream.login}`}>
      <Thumbnail>
        <Image src={stream.thumbnail} />
        <LiveAndViewers>
          <LiveIndicator>
            <LiveIndicatorItems>
              <RedDot />
              <LiveString>LIVE</LiveString>
            </LiveIndicatorItems>
          </LiveIndicator>
          <ViewersBadge>
            <EyesIcon>
              <i className='material-icons'>remove_red_eye</i>
            </EyesIcon>
            {stream.viewer_count}
          </ViewersBadge>
        </LiveAndViewers>
      </Thumbnail>
      <Footer>
        <Avatar>
          <AvatarImg src={stream.avatar} />
        </Avatar>
        <Titles>
          <StreamTitle>
            <TitleString>{stream.title}</TitleString>
          </StreamTitle>
          <StreamerName>{stream.user_name}</StreamerName>
          <GameName>
            <GameString>{stream.login}</GameString>
          </GameName>
        </Titles>
      </Footer>
    </CardContent>
  </Card>
);

export default StreamCard;
