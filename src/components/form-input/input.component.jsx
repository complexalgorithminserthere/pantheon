import React from 'react';

import { FormInputGroup, FormInputField, FormInputLabel } from './input.styles';

const FormInput = ({ label, handleChange, ...otherProps }) => (
  <FormInputGroup>
    <FormInputField onChange={handleChange} {...otherProps} />
    {label ? <FormInputLabel {...otherProps}>{label}</FormInputLabel> : null}
  </FormInputGroup>
);

export default FormInput;
