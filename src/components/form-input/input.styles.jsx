import styled, { css } from 'styled-components';

const getLabelShrinkStyle = props => {
  if (props.value.length) {
    return shrinkLabel;
  }
};

const shrinkLabel = css`
  top: -5px;
  left: 0px;
  font-size: 12px;
  color: #3cc8c8;
`;

export const FormInputGroup = styled.div`
  position: relative;
`;

export const FormInputLabel = styled.label`
  color: rgba(228, 238, 242, 0.6);
  font-size: 16px;
  font-weight: normal;
  position: absolute;
  pointer-events: none;
  left: 10px;
  top: 20px;
  transition: 300ms ease all;

  ${getLabelShrinkStyle}
`;

FormInputLabel.displayName = 'FormInputLabel';

export const FormInputField = styled.input`
  background: none;
  background-color: none;
  color: #ffffff;
  font-size: 18px;
  padding: 8px 10px 8px 5px;
  display: block;
  width: 100%;
  border-radius: 0;
  border: rgba(228, 238, 242, 0.22) solid;
  border-width: 2px 2px 2px 2px;
  margin: 10px 0;

  &:focus {
    outline: none;
    border-color: #3cc8c8;
  }

  &:focus ~ ${FormInputLabel} {
    ${shrinkLabel};
  }
`;

FormInputField.displayName = 'FormInputField';
