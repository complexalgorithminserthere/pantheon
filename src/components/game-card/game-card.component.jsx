import React from 'react';

import {
  Card,
  CardContent,
  Thumbnail,
  Footer,
  Title,
  TruncatedText,
  TextWrapper,
  Repeated
} from './game-card.styles';

const GameCard = ({ game }) => (
  <Card>
    <CardContent to={`/games/${game.id}`}>
      <Thumbnail background={game.art}></Thumbnail>
      <Footer>
        <Title>
          <TruncatedText>
            <TextWrapper>
              {game.name}
              <Repeated>{game.name}</Repeated>
            </TextWrapper>
          </TruncatedText>
        </Title>
      </Footer>
    </CardContent>
  </Card>
);

export default GameCard;
