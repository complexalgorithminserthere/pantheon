import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Card = styled.div`
  flex-basis: 16.66667%;

  @media (max-width: 960px) {
    flex-basis: 25%;
  }

  @media (max-width: 600px) {
    flex-basis: 50%;
  }
`;

export const CardContent = styled(Link)`
  position: relative;
  border-radius: 6px;
  background: #2f3136;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  display: block;
  overflow: hidden;
  margin: 4px;
  color: #0083ef;
  text-decoration: none;
  transition: color 150ms;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border: 2px solid transparent;
    z-index: 1;
    border-radius: 6px;
  }
`;

export const Thumbnail = styled.div`
  background-image: url('${props => props.background}');
  padding-top: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  display: block;
`;

export const Footer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 5px;
  flex-grow: 1;
  letter-spacing: 1px;
`;

export const Title = styled.div`
  font-size: 0.9em;
  font-family: 'Orbitron';
  letter-spacing: 2px;
  color: #fff;
  position: relative;
  height: 1.5em;
  font-weight: 400;
`;

export const TruncatedText = styled.h2`
  position: absolute;
  left: 0;
  right: 0;
  height: 100%;
  font-size: 1em;
  display: block;
  overflow: hidden;
`;

export const TextWrapper = styled.div`
  display: inline-block;
  position: relative;
  white-space: nowrap;
  text-overflow: ellipsis;
  width: 100%;
  overflow: hidden;
  line-height: 1.25em;
`;

export const Repeated = styled.span`
  position: absolute;
  top: 0;
  left: 100%;
  margin-left: 100px;
  display: block;
`;
