import React from 'react';
import { PageContainer, Container } from './stream.styles';
import Stream from '../../components/stream/stream.component';

const StreamPage = ({ match }) => (
  <PageContainer>
    <Container>
      <Stream match={match} />
    </Container>
  </PageContainer>
);

export default StreamPage;
