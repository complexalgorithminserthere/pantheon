import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  fetchSelectedGameStartAsync,
  resetSelectedGames
} from '../../redux/games/games.actions';
import {
  selectSelectedGame,
  selectIsGameAndStreamsLoaded
} from '../../redux/games/games.selectors';
import GameStreams from '../../components/selected-game-streams/selected-game-streams.component';
import GameBanner from '../../components/game-banner/game-banner.component';
import withSpinner from '../../components/with-spinner/with-spinner.component';
const GameBannerWithSpinner = withSpinner(GameBanner);
const GameStreamsWithSpinner = withSpinner(GameStreams);

class SelectedGame extends React.Component {
  componentDidMount() {
    const { fetchGameStreamsAsync, match } = this.props;
    const {
      params: { gameName }
    } = match;
    fetchGameStreamsAsync(gameName);
  }

  componentWillUnmount() {
    const { reset } = this.props;
    reset();
  }

  render() {
    const { isGameAndStreamsLoaded, game } = this.props;
    return (
      <div>
        <GameBannerWithSpinner
          game={game}
          isLoading={!isGameAndStreamsLoaded}
        />
        <GameStreamsWithSpinner isLoading={!isGameAndStreamsLoaded} />
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  game: selectSelectedGame,
  isGameAndStreamsLoaded: selectIsGameAndStreamsLoaded
});

const mapDispatchToProps = dispatch => ({
  fetchGameStreamsAsync: gameId =>
    dispatch(fetchSelectedGameStartAsync(gameId)),
  reset: () => dispatch(resetSelectedGames())
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectedGame);
