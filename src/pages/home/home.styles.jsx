import styled from 'styled-components';

export const PageContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 1rem 0;
`;

export const Container = styled.div`
  margin: 0 auto;
  max-width: 1440px;
  display: block;
  padding-top: 1px;
`;

export const Background = styled.div`
  background-image: linear-gradient(
      to bottom,
      rgba(37, 49, 65, 0.6) 0%,
      #202225 100%
    ),
    url('https://wallpapercave.com/wp/wp5872638.jpg');
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  z-index: -1;

  @media (max-width: 600px) {
    height: 60vh;
  }
`;
