import React from 'react';
import { PageContainer, Container, Background } from './home.styles';
import { connect } from 'react-redux';
import {
  fetchTopStreamsStartAsync,
  fetchNewStreamsStartAsync,
  resetStreams
} from '../../redux/streams/streams.actions';
import { fetchTopGamesStartAsync } from '../../redux/top-games/top-games.actions';
import { createStructuredSelector } from 'reselect';
import {
  selectIsTopStreamsLoaded,
  selectIsNewStreamsLoaded
} from '../../redux/streams/streams.selectors';
import { selectIsTopGamesLoaded } from '../../redux/top-games/top-games.selectors';
import withSpinner from '../../components/with-spinner/with-spinner.component';

import FeaturedCarousel from '../../components/featured-carousel/featured-carousel.component';
import TopGames from '../../components/top-games/top-games.component';
import TopStreams from '../../components/top-streams/top-streams.component';

const FeaturedCarouselWithSpinner = withSpinner(FeaturedCarousel);
const TopGamesWithSpinner = withSpinner(TopGames);
const TopStreamsWithSpinner = withSpinner(TopStreams);

class HomePage extends React.Component {
  componentDidMount() {
    const {
      fetchTopStreamsStartAsync,
      fetchTopGamesAsync,
      fetchNewStreamsAsync
    } = this.props;

    fetchTopStreamsStartAsync();
    fetchNewStreamsAsync();
    fetchTopGamesAsync();
  }

  componentWillUnmount() {
    const { reset } = this.props;

    reset();
  }

  render() {
    const {
      isTopStreamsLoaded,
      isTopGamesLoaded,
      isNewStreamsLoaded
    } = this.props;
    return (
      <Container>
        <Background />
        <PageContainer>
          <FeaturedCarouselWithSpinner isLoading={!isTopStreamsLoaded} />
          <TopGamesWithSpinner isLoading={!isTopGamesLoaded} />
          <TopStreamsWithSpinner isLoading={!isNewStreamsLoaded} />
        </PageContainer>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  isTopStreamsLoaded: selectIsTopStreamsLoaded,
  isTopGamesLoaded: selectIsTopGamesLoaded,
  isNewStreamsLoaded: selectIsNewStreamsLoaded
});

const mapDispatchToProps = dispatch => ({
  fetchTopStreamsStartAsync: () => dispatch(fetchTopStreamsStartAsync()),
  fetchTopGamesAsync: () => dispatch(fetchTopGamesStartAsync()),
  fetchNewStreamsAsync: () => dispatch(fetchNewStreamsStartAsync()),
  reset: () => dispatch(resetStreams())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
