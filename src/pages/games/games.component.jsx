import React from 'react';
import { Route } from 'react-router-dom';
import { PageContainer, Container } from './games.styles';
import GamesGrid from '../../components/games/games.component';
import SelectedGame from '../selected-game/selected-game.component';

class Games extends React.Component {
  render() {
    const { match } = this.props;
    return (
      <PageContainer>
        <Container>
          <Route exact path={`${match.path}`} component={GamesGrid} />
          <Route path={`${match.path}/:gameName`} component={SelectedGame} />
        </Container>
      </PageContainer>
    );
  }
}

export default Games;
