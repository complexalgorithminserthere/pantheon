import styled from 'styled-components';

export const PageContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  padding: 1rem 0;
`;

export const Container = styled.div`
  margin: 0 auto;
  max-width: 1440px;
  display: block;
  padding-top: 1px;
`;