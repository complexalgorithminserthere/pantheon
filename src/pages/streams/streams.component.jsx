import React from 'react';
import { connect } from 'react-redux';
import { PageContainer, Container } from './streams.styles';
import TopStreams from '../../components/top-streams/top-streams.component';
import withSpinner from '../../components/with-spinner/with-spinner.component';
import {
  fetchNewStreamsStartAsync,
  resetStreams
} from '../../redux/streams/streams.actions';
import { createStructuredSelector } from 'reselect';
import { selectIsNewStreamsLoaded } from '../../redux/streams/streams.selectors';

const TopStreamsWithSpinner = withSpinner(TopStreams);

class Streams extends React.Component {
  componentDidMount() {
    const { fetchNewStreamsAsync } = this.props;

    fetchNewStreamsAsync();
  }

  componentWillUnmount() {
    const { reset } = this.props;

    reset();
  }

  render() {
    const { isNewStreamsLoaded } = this.props;
    return (
      <PageContainer>
        <Container>
          <TopStreamsWithSpinner isLoading={!isNewStreamsLoaded} />
        </Container>
      </PageContainer>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  isNewStreamsLoaded: selectIsNewStreamsLoaded
});

const mapDispatchToProps = dispatch => ({
  fetchNewStreamsAsync: () => dispatch(fetchNewStreamsStartAsync()),
  reset: () => dispatch(resetStreams())
});

export default connect(mapStateToProps, mapDispatchToProps)(Streams);
