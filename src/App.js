import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/header.component';
import ModalConductor from './components/modal-conductor/modal-conductor.component';
import { selectModalVisible } from './redux/user/user.selectors';

//Pages
import HomePage from './pages/home/home.component';
import Streams from './pages/streams/streams.component';
import Games from './pages/games/games.component';
import Stream from './pages/stream/stream.component';

import { scrollObserver } from './utils/header/scrollObserver';

import './App.css';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      scrolled: false
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    scrollObserver(this);
  };

  render() {
    const user = this.state.user;
    const nonUser = this.state.nonUser;
    const isLoggedIn = this.state.isLoggedIn;
    return (
      <div className='app'>
        {isLoggedIn ? (
          <Header
            user={user}
            handleLogout={this.logout}
            scrolled={this.state.scrolled}
          />
        ) : (
          <Header nonUser={nonUser} scrolled={this.state.scrolled} />
        )}

        {this.props.modalVisible ? <ModalConductor /> : null}
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route path='/streams' component={Streams} />
          <Route path='/games' component={Games} />
          <Route path='/stream/:channel' component={Stream} />
        </Switch>
        {/* <p>{this.state.user.name}</p> */}
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  modalVisible: selectModalVisible
});

export default connect(mapStateToProps)(App);
