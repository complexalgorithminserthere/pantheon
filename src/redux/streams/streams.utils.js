export const carouselNavigate = (arr, n) => {
  return arr.splice(n).concat(arr);
};

export const prepareStreams = (currentStreams, streamsToAdd) => {
  const mappedStreams = streamsToAdd.map(({ thumbnail_url, ...rest }) => ({
    ...rest,
    thumbnail: thumbnail_url.replace(/{width}x{height}/gi, '450x253')
  }));

  return !currentStreams
    ? currentStreams.push(mappedStreams)
    : currentStreams.concat(mappedStreams);
};
