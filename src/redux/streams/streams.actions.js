import StreamActionTypes from './streams.types';
import axios from 'axios';
import { headers } from '../../utils/header';

export const fetchTopStreamsStart = () => ({
  type: StreamActionTypes.FETCH_TOP_STREAMS_START,
});

export const fetchTopStreamsSuccess = (streams) => ({
  type: StreamActionTypes.FETCH_TOP_STREAMS_SUCCESS,
  payload: streams,
});

export const fetchTopStreamsFail = (errorMessage) => ({
  type: StreamActionTypes.FETCH_TOP_STREAMS_FAIL,
  payload: errorMessage,
});

export const fetchTopStreamsStartAsync = () => {
  return async (dispatch) => {
    try {
      
      const url = 'https://api.twitch.tv/helix/streams?first=5';
      const userUrl = 'https://api.twitch.tv/helix/users?';
      let userIds = '';
      dispatch(fetchTopStreamsStart());

      const response = await axios.get(url, { headers });
      const topStreams = response.data.data;

      topStreams.forEach((stream) => (userIds += `id=${stream.user_id}&`));
      userIds = userIds.slice(0, -1);

      const userResponse = await axios.get(userUrl + userIds, { headers });
      const users = userResponse.data.data;

      const completeStreams = topStreams.map((stream) => {
        stream.avatar = users.find(
          (user) => user.id === stream.user_id
        ).profile_image_url;
        return stream;
      });

      const mappedStreams = completeStreams.map(
        ({ thumbnail_url, ...rest }) => ({
          ...rest,
          thumbnail: thumbnail_url.replace(/{width}x{height}/gi, '1280x720'),
        })
      );

      dispatch(fetchTopStreamsSuccess(mappedStreams));
    } catch (error) {
      dispatch(fetchTopStreamsFail(error.message));
    }
  };
};

export const fetchNewStreamsStart = () => ({
  type: StreamActionTypes.FETCH_NEW_STREAMS_START,
});

export const fetchNewStreamsSuccess = (streams) => ({
  type: StreamActionTypes.FETCH_NEW_STREAMS_SUCCESS,
  payload: streams,
});

export const fetchNewStreamsCursor = (cursor) => ({
  type: StreamActionTypes.FETCH_NEW_STREAMS_CURSOR,
  payload: cursor,
});

export const fetchNewStreamsFail = (errorMessage) => ({
  type: StreamActionTypes.FETCH_NEW_STREAMS_FAIL,
  payload: errorMessage,
});

export const fetchNewStreamsStartAsync = () => {
  return async (dispatch, getState) => {
    try {
      

      const userUrl = 'https://api.twitch.tv/helix/users?';
      let url;
      let userIds = '';
      let currentCursor = getState().streams.cursor;

      if (!currentCursor) {
        url = 'https://api.twitch.tv/helix/streams?first=20';
      } else {
        url = `https://api.twitch.tv/helix/streams?first=20&after=${currentCursor}`;
      }

      dispatch(fetchNewStreamsStart());
      const response = await axios.get(url, { headers });

      const {
        data: {
          pagination: { cursor },
        },
      } = response;
      dispatch(fetchNewStreamsCursor(cursor));

      const streams = response.data.data;
      streams.forEach((stream) => (userIds += `id=${stream.user_id}&`));
      userIds = userIds.slice(0, -1);

      const userResponse = await axios.get(userUrl + userIds, { headers });
      const users = userResponse.data.data;

      const completeStreams = streams.map((stream) => {
        stream.avatar = users.find(
          (user) => user.id === stream.user_id
        ).profile_image_url;
        stream.login = users.find((user) => user.id === stream.user_id).login;
        return stream;
      });

      dispatch(fetchNewStreamsSuccess(completeStreams));
    } catch (error) {
      dispatch(fetchNewStreamsFail(error.message));
      console.log(error);
    }
  };
};

export const resetStreams = () => ({
  type: StreamActionTypes.RESET_STREAMS,
});

export const carouselNavigate = (number) => ({
  type: StreamActionTypes.CAROUSEL_NAVIGATION,
  payload: number,
});
