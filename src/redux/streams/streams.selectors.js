import { createSelector } from 'reselect';

const selectStreams = state => state.streams;

//Top Streams for Carousel

export const selectTopStreams = createSelector(
  [selectStreams],
  streams => streams.topStreams
);

export const selectIsTopStreamsLoaded = createSelector(
  [selectStreams],
  streams => !!streams.topStreams
);

//New streams piepline

export const selectNewStreams = createSelector(
  [selectStreams],
  streams => streams.newStreams
);

export const selectIsNewStreamsLoaded = createSelector(
  [selectStreams],
  streams => !!streams.newStreams
);
