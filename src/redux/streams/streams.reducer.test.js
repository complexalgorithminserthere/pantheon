import StreamActionTypes from './streams.types';
import streamsReducer from './streams.reducer';

const initialState = {
  topStreams: null,
  newStreams: [],
  isFetching: false,
  isAllStreamsFetching: false,
  errorMessage: null,
  allStreamsErrorMessage: null,
  cursor: ''
};

describe('streamsReducer', () => {
  it('should return initial state', () => {
    expect(streamsReducer(undefined, {})).toEqual(initialState);
  });

  it('should set isFetching to true if fetchTopStreamsStart action', () => {
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_TOP_STREAMS_START
      }).isFetching
    ).toBe(true);
  });

  it('should set isFetching to false and topStreams to payload if fetchTopStreamsSuccess', () => {
    const mockStreams = [{ id: 1 }, { id: 2 }, { id: 3 }];
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_TOP_STREAMS_SUCCESS,
        payload: mockStreams
      })
    ).toEqual({
      ...initialState,
      isFetching: false,
      topStreams: mockStreams
    });
  });

  it('should set isFetching to false and errorMessage to payload if fetchTopStreamsFail', () => {
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_TOP_STREAMS_FAIL,
        payload: 'error'
      })
    ).toEqual({
      ...initialState,
      isFetching: false,
      errorMessage: 'error'
    });
  });

  it('should set isAllStreamsFetching to true if fetchNewStreamsStart action', () => {
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_NEW_STREAMS_START
      }).isAllStreamsFetching
    ).toBe(true);
  });

  it('should set cursor to payload if fetchNewStreamsCursor action', () => {
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_NEW_STREAMS_CURSOR,
        payload: 'fjbvkjbvskfv'
      })
    ).toEqual({
      ...initialState,
      cursor: 'fjbvkjbvskfv'
    });
  });

  it('should set newStreams to payload and isAllStreamsFetching to false if fetchNewStreamsSuccess and newStreams is empty', () => {
    const mockStreams = [
      { id: 1, thumbnail_url: 'hgv{width}x{height}' },
      { id: 2, thumbnail_url: 'jvjh{width}x{height}' },
      { id: 3, thumbnail_url: 'hgh{width}x{height}' }
    ];
    const mockPreparedStreams = [
      { id: 1, thumbnail: 'hgv450x253' },
      { id: 2, thumbnail: 'jvjh450x253' },
      { id: 3, thumbnail: 'hgh450x253' }
    ];
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_NEW_STREAMS_SUCCESS,
        payload: mockStreams
      })
    ).toEqual({
      ...initialState,
      isAllStreamsFetching: false,
      newStreams: mockPreparedStreams
    });
  });

  it('should set isAllStreamsFetching to false and concatenate payload streams to newStreams if not empty', () => {
    const existingStreams = [
      { id: 1, thumbnail: 'hgv450x253' },
      { id: 2, thumbnail: 'jvjh450x253' },
      { id: 3, thumbnail: 'hgh450x253' }
    ];

    const mockNewStreams = [
      { id: 4, thumbnail_url: 'hgv{width}x{height}' },
      { id: 5, thumbnail_url: 'jvjh{width}x{height}' },
      { id: 6, thumbnail_url: 'hgh{width}x{height}' }
    ];

    const mockNewPreparedStreams = [
      { id: 4, thumbnail: 'hgv450x253' },
      { id: 5, thumbnail: 'jvjh450x253' },
      { id: 6, thumbnail: 'hgh450x253' }
    ];

    const mockPrevState = {
      topStreams: [],
      newStreams: existingStreams,
      isFetching: false,
      isAllStreamsFetching: false,
      errorMessage: null,
      allStreamsErrorMessage: null,
      cursor: ''
    };

    expect(
      streamsReducer(mockPrevState, {
        type: StreamActionTypes.FETCH_NEW_STREAMS_SUCCESS,
        payload: mockNewStreams
      })
    ).toEqual({
      ...mockPrevState,
      isAllStreamsFetching: false,
      newStreams: [...existingStreams, ...mockNewPreparedStreams]
    });
  });

  it('should reset newStreams and cursor states if resetStreams action', () => {
    const existingStreams = [
      { id: 1, thumbnail: 'hgv450x253' },
      { id: 2, thumbnail: 'jvjh450x253' },
      { id: 3, thumbnail: 'hgh450x253' }
    ];

    const mockPrevState = {
      topStreams: [],
      newStreams: existingStreams,
      isFetching: false,
      isAllStreamsFetching: false,
      errorMessage: null,
      allStreamsErrorMessage: null,
      cursor: 'gvjhvjvjvgvjgv'
    };

    expect(
      streamsReducer(mockPrevState, {
        type: StreamActionTypes.RESET_STREAMS
      })
    ).toEqual({
      cursor: '',
      newStreams: []
    });
  });

  it('should set isAllStreamsFetching to false and allStreamsErrorMessage to payload if fetchNewStreamsFail', () => {
    expect(
      streamsReducer(initialState, {
        type: StreamActionTypes.FETCH_NEW_STREAMS_FAIL,
        payload: 'error'
      })
    ).toEqual({
      ...initialState,
      isAllStreamsFetching: false,
      allStreamsErrorMessage: 'error'
    });
  });
});
