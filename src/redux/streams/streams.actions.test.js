import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import ActionTypes from './streams.types';

import {
  fetchTopStreamsStart,
  fetchTopStreamsStartAsync
} from './streams.actions';

const mockStore = configureMockStore([thunk]);

describe('thunks', () => {
  describe('fetchTopStreamsStartAsync', () => {
    beforeEach(() => {
      moxios.install();
    });

    afterEach(() => {
      moxios.uninstall();
    });
    it('creates both fetchTopStreamsStart and fetchTopStreamsSuccess when api calls succeeds', async () => {
      const streamsResponse = [
        { user_id: 1, thumbnail_url: 'thumbnail-1-{width}x{height}' },
        { user_id: 2, thumbnail_url: 'thumbnail-2-{width}x{height}' },
        { user_id: 3, thumbnail_url: 'thumbnail-3-{width}x{height}' }
      ];
      const usersResponse = [
        { id: 1, profile_image_url: 'image-1' },
        { id: 2, profile_image_url: 'image-2' },
        { id: 3, profile_image_url: 'image-3' }
      ];

      moxios.stubRequest('https://api.twitch.tv/helix/streams?first=5', {
        status: 200,
        response: { data: streamsResponse }
      });

      // Mock the second request.
      moxios.stubRequest('https://api.twitch.tv/helix/users?id=1&id=2&id=3', {
        status: 200,
        response: { data: usersResponse }
      });

      const store = mockStore();

      await store.dispatch(fetchTopStreamsStartAsync());

      expect(store.getActions()).toEqual([
        fetchTopStreamsStart(),
        {
          type: ActionTypes.FETCH_TOP_STREAMS_SUCCESS,
          payload: [
            {
              avatar: 'image-1',
              thumbnail: 'thumbnail-1-1280x720',
              user_id: 1
            },
            {
              avatar: 'image-2',
              thumbnail: 'thumbnail-2-1280x720',
              user_id: 2
            },
            {
              avatar: 'image-3',
              thumbnail: 'thumbnail-3-1280x720',
              user_id: 3
            }
          ]
        }
      ]);
    });

    it('creates both fetchTopStreamsStart and fetchTopStreamsFail when api calls fail', async () => {
      moxios.stubRequest('https://api.twitch.tv/helix/streams?first=5', {
        status: 200,
        response: 'No data'
      });

      const store = mockStore();

      await store.dispatch(fetchTopStreamsStartAsync());

      expect(store.getActions()).toEqual([
        fetchTopStreamsStart(),
        {
          type: ActionTypes.FETCH_TOP_STREAMS_FAIL,
          payload: "Cannot read property 'forEach' of undefined"
        }
      ]);
    });
  });
});
