import StreamActionTypes from './streams.types';
import { carouselNavigate, prepareStreams } from './streams.utils';

const INITIAL_STATE = {
  topStreams: null,
  newStreams: [],
  isFetching: false,
  isAllStreamsFetching: false,
  errorMessage: null,
  allStreamsErrorMessage: null,
  cursor: ''
};

const streamsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case StreamActionTypes.FETCH_TOP_STREAMS_START:
      return {
        ...state,
        isFetching: true
      };
    case StreamActionTypes.FETCH_TOP_STREAMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        topStreams: action.payload
      };
    case StreamActionTypes.FETCH_TOP_STREAMS_FAIL:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload
      };
    case StreamActionTypes.CAROUSEL_NAVIGATION:
      return {
        ...state,
        topStreams: carouselNavigate(state.topStreams, action.payload)
      };
    case StreamActionTypes.FETCH_NEW_STREAMS_START:
      return {
        ...state,
        isAllStreamsFetching: true
      };
    case StreamActionTypes.FETCH_NEW_STREAMS_SUCCESS:
      return {
        ...state,
        isAllStreamsFetching: false,
        newStreams: prepareStreams(state.newStreams, action.payload)
      };
    case StreamActionTypes.FETCH_NEW_STREAMS_CURSOR:
      return {
        ...state,
        cursor: action.payload
      };
    case StreamActionTypes.RESET_STREAMS:
      return {
        newStreams: [],
        cursor: ''
      };
    case StreamActionTypes.FETCH_NEW_STREAMS_FAIL:
      return {
        ...state,
        allStreamsErrorMessage: action.payload
      };
    default:
      return state;
  }
};

export default streamsReducer;
