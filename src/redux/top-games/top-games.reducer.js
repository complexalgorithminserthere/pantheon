import TopGamesActionTypes from './top-games.types';

const INITIAL_STATE = {
  topGames: null,
  isFetching: false,
  errorMessage: null
};

const topGamesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TopGamesActionTypes.FETCH_TOP_GAMES_START:
      return {
        ...state,
        isFetching: true
      };
    case TopGamesActionTypes.FETCH_TOP_GAMES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        topGames: action.payload
      };
    case TopGamesActionTypes.FETCH_TOP_GAMES_FAIL:
      return {
        ...state,
        errorMessage: action.payload
      };
    default:
      return state;
  }
};

export default topGamesReducer;
