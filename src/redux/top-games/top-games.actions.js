import TopGamesActionTypes from './top-games.types';
import axios from 'axios';
import { headers } from '../../utils/header';

export const fetchTopGamesStart = () => ({
  type: TopGamesActionTypes.FETCH_TOP_GAMES_START
});

export const fetchTopGamesSuccess = games => ({
  type: TopGamesActionTypes.FETCH_TOP_GAMES_SUCCESS,
  payload: games
});

export const fetchTopGamesFail = errorMessage => ({
  type: TopGamesActionTypes.FETCH_TOP_GAMES_FAIL,
  payload: errorMessage
});

export const fetchTopGamesStartAsync = () => {
  return dispatch => {
    
    const url = 'https://api.twitch.tv/helix/games/top';

    dispatch(fetchTopGamesStart());

    axios
      .get(url, { headers })
      .then(res => {
        let games = res.data.data
          .filter(game => game.name !== 'Just Chatting')
          .slice(0, 6);
        dispatch(fetchTopGamesSuccess(games));
      })
      .catch(err => dispatch(fetchTopGamesFail(err.message)));
  };
};
