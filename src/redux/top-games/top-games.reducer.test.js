import TopGamesActionTypes from './top-games.types';
import topGamesReducer from './top-games.reducer';

const initialState = {
  topGames: null,
  isFetching: false,
  errorMessage: null
};

describe('topGamesReducer', () => {
  it('should return initial state', () => {
    expect(topGamesReducer(undefined, {})).toEqual(initialState);
  });

  it('should set isFetching to true if fetchTopGamesStart action', () => {
    expect(
      topGamesReducer(initialState, {
        type: TopGamesActionTypes.FETCH_TOP_GAMES_START
      }).isFetching
    ).toBe(true);
  });

  it('should set isFetching to false and topGames to payload if fetchTopGamesSuccess', () => {
    const mockGames = [{ id: 1 }, { id: 2 }, { id: 3 }];
    expect(
      topGamesReducer(initialState, {
        type: TopGamesActionTypes.FETCH_TOP_GAMES_SUCCESS,
        payload: mockGames
      })
    ).toEqual({
      ...initialState,
      isFetching: false,
      topGames: mockGames
    });
  });

  it('should set isFetching to false and errorMessage to payload if fetchTopGamesFail', () => {
    expect(
      topGamesReducer(initialState, {
        type: TopGamesActionTypes.FETCH_TOP_GAMES_FAIL,
        payload: 'error'
      })
    ).toEqual({
      ...initialState,
      isFetching: false,
      errorMessage: 'error'
    });
  });
});
