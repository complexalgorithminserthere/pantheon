import { createSelector } from 'reselect';

const selectTopGamess = state => state.topGames;

export const selectTopGamesInitial = createSelector(
  [selectTopGamess],
  games => games.topGames
);

export const selectTopGames = createSelector([selectTopGamesInitial], games =>
  games.map(({ box_art_url, ...rest }) => ({
    ...rest,
    art: box_art_url.replace(/{width}x{height}/gi, '500x500')
  }))
);

export const selectIsTopGamesLoaded = createSelector(
  [selectTopGamess],
  games => !!games.topGames
);
