import { combineReducers } from 'redux';

import userReducer from './user/user.reducer';
import streamsReducer from './streams/streams.reducer';
import topGamesReducer from './top-games/top-games.reducer';
import gamesReducer from './games/games.reducer';

export default combineReducers({
  user: userReducer,
  streams: streamsReducer,
  topGames: topGamesReducer,
  games: gamesReducer
});
