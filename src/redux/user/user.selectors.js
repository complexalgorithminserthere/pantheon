import { createSelector } from 'reselect';

const selectUser = state => state.user;

export const selectModalVisible = createSelector(
  [selectUser],
  user => user.modalVisible
);

export const selectCurrentModal = createSelector(
  [selectUser],
  user => user.currentModal
);
