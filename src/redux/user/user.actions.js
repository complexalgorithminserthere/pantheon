import UserActionTypes from './user.types';

export const toggleModalVisible = () => ({
  type: UserActionTypes.TOGGLE_MODAL_VISIBLE
});

export const launchSignUp = () => ({
  type: UserActionTypes.LAUNCH_SIGN_UP
});

export const closeOnOutsideClick = () => ({
  type: UserActionTypes.CLOSE_ON_OUTSIDE_CLICK
});
