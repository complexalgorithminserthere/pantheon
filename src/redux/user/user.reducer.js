import UserActionTypes from './user.types';

const INITIAL_STATE = {
  currentUser: null,
  modalVisible: false,
  currentModal: ''
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UserActionTypes.TOGGLE_MODAL_VISIBLE:
      return {
        ...state,
        modalVisible: !state.modalVisible
      };
    case UserActionTypes.LAUNCH_SIGN_UP:
      return {
        ...state,
        currentModal: 'sign-up',
        modalVisible: !state.modalVisible
      };
    case UserActionTypes.CLOSE_ON_OUTSIDE_CLICK:
      return {
        ...state,
        modalVisible: !state.modalVisible,
        currentModal: ''
      };
    default:
      return state;
  }
};

export default userReducer;
