import GamesActionTypes from './games.types';
import { prepareGames, prepareStreams } from './games.utils';

const INITIAL_STATE = {
  games: [],
  selectedGames: [],
  isFetching: false,
  isSelectedGameFetching: false,
  errorMessage: null,
  selectedGame: null,
  cursor: '',
  selectedGameCursor: ''
};

const gamesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GamesActionTypes.FETCH_GAMES_START:
      return {
        ...state,
        isFetching: true
      };
    case GamesActionTypes.FETCH_GAMES_SUCCESS:
      return {
        ...state,
        games: prepareGames(state.games, action.payload),
        isFetching: false
      };
    case GamesActionTypes.FETCH_GAMES_FAIL:
      return {
        ...state,
        errorMessage: action.payload,
        isFetching: false
      };
    case GamesActionTypes.FETCH_GAMES_CURSOR:
      return {
        ...state,
        cursor: action.payload
      };
    case GamesActionTypes.RESET_GAMES:
      return {
        ...state,
        games: [],
        cursor: ''
      };
    case GamesActionTypes.SET_SELECTED_GAME:
      return {
        ...state,
        selectedGame: action.payload
      };
    case GamesActionTypes.FETCH_SELECTED_GAME_START:
      return {
        ...state,
        isSelectedGameFetching: true
      };
    case GamesActionTypes.FETCH_SELECTED_GAME_SUCCESS:
      return {
        ...state,
        isSelectedGameFetching: false,
        selectedGames: prepareStreams(state.selectedGames, action.payload)
      };
    case GamesActionTypes.FETCH_SELECTED_GAME_FAIL:
      return {
        ...state,
        errorMessage: action.payload,
        isSelectedGameFetching: false
      };
    case GamesActionTypes.FETCH_SELECTED_GAME_CURSOR:
      return {
        ...state,
        selectedGameCursor: action.payload
      };
    case GamesActionTypes.RESET_SELECTED_GAMES:
      return {
        ...state,
        selectedGames: [],
        selectedGameCursor: '',
        selectedGame: null
      };
    default:
      return state;
  }
};

export default gamesReducer;
