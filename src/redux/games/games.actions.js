import GamesActionTypes from './games.types';
import axios from 'axios';
import { headers } from '../../utils/header';

export const fetchGamesStart = () => ({
  type: GamesActionTypes.FETCH_GAMES_START 
});

export const fetchGamesSuccess = games => ({
  type: GamesActionTypes.FETCH_GAMES_SUCCESS,
  payload: games
});

export const fetchGamesFail = errorMessage => ({
  type: GamesActionTypes.FETCH_GAMES_FAIL,
  payload: errorMessage
});

export const fetchGamesCursor = cursor => ({
  type: GamesActionTypes.FETCH_GAMES_CURSOR,
  payload: cursor
});

export const resetGames = () => ({
  type: GamesActionTypes.RESET_GAMES
});

export const fetchGamesStartAsync = () => {
  return async (dispatch, getState) => {
    try {
      

      let url;
      let currentCursor = getState().games.cursor;

      if (!currentCursor) {
        url = 'https://api.twitch.tv/helix/games/top?first=24';
      } else {
        url = `https://api.twitch.tv/helix/games/top?first=24&after=${currentCursor}`;
      }

      dispatch(fetchGamesStart());
      const response = await axios.get(url, { headers });

      const {
        data: {
          pagination: { cursor }
        }
      } = response;

      dispatch(fetchGamesCursor(cursor));

      const games = response.data.data;

      dispatch(fetchGamesSuccess(games));
    } catch (error) {
      dispatch(fetchGamesFail(error.message));
      console.log(error);
    }
  };
};

export const setSelectedGame = game => ({
  type: GamesActionTypes.SET_SELECTED_GAME,
  payload: { ...game }
});

// Selected game streams fetch

export const fetchSelectedGameStart = () => ({
  type: GamesActionTypes.FETCH_SELECTED_GAME_START
});

export const fetchSelectedGameSuccess = streams => ({
  type: GamesActionTypes.FETCH_SELECTED_GAME_SUCCESS,
  payload: streams
});

export const fetchSelectedGameFail = errorMessage => ({
  type: GamesActionTypes.FETCH_SELECTED_GAME_FAIL,
  payload: errorMessage
});

export const fetchSelectedGameCursor = cursor => ({
  type: GamesActionTypes.FETCH_SELECTED_GAME_CURSOR,
  payload: cursor
});

export const fetchSelectedGameStartAsync = gameId => {
  return async (dispatch, getState) => {
    try {
      

      //Get selected game
      let gameUrl = `https://api.twitch.tv/helix/games?id=${gameId}`;

      const gameResponse = await axios.get(gameUrl, { headers });
      const gameData = gameResponse.data.data;
      const getGame = gameData.map(({ box_art_url, ...rest }) => ({
        ...rest,
        art: box_art_url.replace(/{width}x{height}/gi, '500x500')
      }));
      dispatch(setSelectedGame(getGame[0]));

      let currentCursor = getState().games.selectedGameCursor;
      const userUrl = 'https://api.twitch.tv/helix/users?';
      let userIds = '';
      let url;

      if (!currentCursor) {
        url = `https://api.twitch.tv/helix/streams?game_id=${gameId}&first=20`;
      } else {
        url = `https://api.twitch.tv/helix/streams?game_id=${gameId}&first=20&after=${currentCursor}`;
      }

      dispatch(fetchSelectedGameStart());
      const response = await axios.get(url, { headers });
      const {
        data: {
          pagination: { cursor }
        }
      } = response;
      dispatch(fetchSelectedGameCursor(cursor));

      const streams = response.data.data;
      streams.forEach(stream => (userIds += `id=${stream.user_id}&`));
      userIds = userIds.slice(0, -1);

      const userResponse = await axios.get(userUrl + userIds, { headers });
      const users = userResponse.data.data;

      const completeStreams = streams.map(stream => {
        stream.avatar = users.find(
          user => user.id === stream.user_id
        ).profile_image_url;
        stream.login = users.find(user => user.id === stream.user_id).login;
        return stream;
      });

      dispatch(fetchSelectedGameSuccess(completeStreams));
    } catch (error) {
      dispatch(fetchSelectedGameFail(error.message));
      console.log(error);
    }
  };
};

export const resetSelectedGames = () => ({
  type: GamesActionTypes.RESET_SELECTED_GAMES
});
