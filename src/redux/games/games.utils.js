export const prepareGames = (currentGames, gamesToAdd) => {
  const mappedGames = gamesToAdd.map(({ box_art_url, ...rest }) => ({
    ...rest,
    art: box_art_url.replace(/{width}x{height}/gi, '500x500')
  }));

  return !currentGames
    ? currentGames.push(mappedGames)
    : currentGames.concat(mappedGames);
};

export const prepareStreams = (currentStreams, streamsToAdd) => {
  const mappedStreams = streamsToAdd.map(({ thumbnail_url, ...rest }) => ({
    ...rest,
    thumbnail: thumbnail_url.replace(/{width}x{height}/gi, '450x253')
  }));

  return !currentStreams
    ? currentStreams.push(mappedStreams)
    : currentStreams.concat(mappedStreams);
};
