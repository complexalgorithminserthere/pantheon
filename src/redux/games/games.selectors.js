import { createSelector } from 'reselect';

const selectGamesState = state => state.games;

export const selectGames = createSelector(
  [selectGamesState],
  games => games.games
);

export const selectFilteredGames = createSelector([selectGames], games =>
  games.filter(game => game.name !== 'Just Chatting')
);

export const selectIsGamesLoaded = createSelector(
  [selectGamesState],
  games => !!games.games
);

//Selected Game

export const selectSelectedGameStreams = createSelector(
  [selectGamesState],
  games => games.selectedGames
);

export const selectSelectedGame = createSelector(
  [selectGamesState],
  games => games.selectedGame
);

export const selectIsGameAndStreamsLoaded = createSelector(
  [selectSelectedGameStreams, selectSelectedGame],
  (streams, game) => !!streams && !!game
);
